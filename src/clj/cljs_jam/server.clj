(ns cljs-jam.server
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.resource :as resources]
            [ring.util.response :as response]
            [hiccup.core :refer [html]]
            [garden.core :refer [css]])
  (:gen-class))

(defn render-app []
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body
   (html [:html
          [:head
           [:link {:rel "stylesheet" :href "style.css"}]]
          [:body#body
           ;; [:div {:align "center"}
           [:canvas#mycanvas {:width 512 :height 364}]
           ;;  ]
           ;; [:div#game-area
           ;;  [:div#canvas]
           ;;  ]
           [:script {:src "js/pixi.dev.js"}]
           [:script {:src "js/Font.js"}]
           [:script {:src "js/game.js"}]
           ]])})

(defn render-css []
  {:status 200
   :headers {"Content-Type" "text/css"}
   :body
   (css [:.sprite
         {
          :position "absolute"
          :background-color "transparent"
          :background-repeat "no-repeat"
          :display "block"
          :height "64px"
          :width "64px"
          :background-position "0px 0"
          }]

        [(keyword "@font-face")
         {
          :font-family "vectorfuture"
          :src "url('/img/ui_space/Fonts/kenvector_future_thin.ttf')"
          }]

        [:body
         {
          :width "100%"
          :height "100%"
          :margin "0"
          :padding "0"
          :background-color "#000000"
          :border "none"

          ;; no scrollbars
          :overflow "hidden"}]

        [:#game-area
         {
          :position "absolute"
          :width "100%"
          :height "100%"
          :left "0"
          :top "0"}]

        [:#ui
         {
          :position "absolute"
          :left "50%"
          :top "50%"
          }]

        [:#status-bottom
         {:position "absolute"
          :width "100%"
          :height "20%"
          :font-size "24pt"
          :bottom "0"
          :opacity "0.8"
          :z-index 200}]

        [:#fps
         {:position "absolute"
          :right 0
          :top 0
          :padding-right 15
          :color "white"
          :z-index 200}]
         )})

(defn handler [request]
  (let [uri (:uri request)]
    (println (name (:request-method request)) uri)
    (cond ;; (= "/help.html" uri)
          ;; (response/redirect "/help.html")

          (= "/style.css" uri)
          (render-css)

          (= "/" uri)
          (render-app)

          :else
          {:status 404
           :headers {"Content-Type" "text/plain"}
           :body "404 Not Found"})))

(def app
  (-> handler
    (resources/wrap-resource "public")))

(defn -main [& args]
  (jetty/run-jetty app {:port 8000}))

(defn -write-files [& args]
  (spit "target/index.html" (:body (render-app)))
  (spit "target/style.css" (:body (render-css))))
