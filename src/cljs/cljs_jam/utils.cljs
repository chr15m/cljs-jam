(ns cljs-jam.utils
  (:require [cljs-jam.events :as events]
            [cljs.core.async :refer [put! chan <! >! alts! timeout close!]]
            [dommy.core :refer-macros [sel1]]
            [clojure.browser.repl :as repl])
  (:require-macros [cljs.core.async.macros :refer [go]]))

;; the following connects to the nrepl server
;; to run repl, lein repl like normal, or cider-jack-in
;; then execute the following two lines in your repl
;;
;; (require 'cljs.repl.browser)
;; (cemerick.piggieback/cljs-repl :repl-env (cljs.repl.browser/repl-env :port 9000))
;;
;; now load the web page. it should attempt connect to the repl back end
;;
;; now evaluate your code in the repl
;;
;; you can reload the browser and keep the cljs nrepl open
(repl/connect "http://localhost:9000/repl")


;;
;; No host methods (static or not) can be used with partial as-is because partial only works with Clojure functions.
;; What you really want is to use clojure.contrib.import-static to easily wrap those static methods:
;;
;; - Chas Emerick
;;
(defn log
  ([a1]
   (.log js/console a1))
  ([a1 a2]
   (.log js/console a1 a2))
  ([a1 a2 a3]
   (.log js/console a1 a2 a3))
  ([a1 a2 a3 a4]
   (.log js/console a1 a2 a3 a4)))
